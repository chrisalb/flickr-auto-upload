var FlickrSearch = require("./modules/flickr");
require(process.env['HOME'] + "/configs/apps/flickr-upload.js");

FlickrSearch.set_config(
    {
        api_key: process.env.flickr_api_key,
        secret: process.env.flickr_secret,
        access_token: process.env.flickr_access_token,
        access_token_secret: process.env.flickr_access_token_secret,

    }
);

FlickrSearch.search(process.argv[3], process.argv[2]);