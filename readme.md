# Upload app for Flickr

Watches directory for changes and uploads new photos automatically to Flickr.

## Requirements
### You need
* Flickr account
* [Flickr app](https://www.flickr.com/services/apps/create/)

## Install flickrapi
```
npm install flickr-auto-upload
```

## Get access tokens from Flickr
```javascript
var Flickr = require("flickrapi"),
    flickrOptions = {
      api_key: "API key that you get from Flickr",
      secret: "API key secret that you get from Flickr",,
      permissions: "delete"
    };

	Flickr.authenticate(flickrOptions, function(error, flickr) {

	});
```

## Usage
``` javascript
var findFiles 		= require("flickr-auto-upload");

findFiles.set_config(
	{
		api_key: "",
		secret: "",
		access_token: "",
		access_token_secret: ""
	}
);

findFiles.watch(
	{
		path: 	'/path/to/images'
	}
);
```


## Limitations
Number of files the directory that you point to for watching has limit depending on your system settings.

## Todo
* Implement queueing if no network is reachable
* Patterns for filetypes
* Write tests

## Mac osx

Edit flickrupload.plist with correct values

### Install to run watcher in background for mac osx
```
$ launchctl load flickrupload.plist

$ launchctl unload flickrupload.plist
```

## Windows
Not tested but have a try with node-windows, https://github.com/coreybutler/node-windows

## License

### ISC

Copyright (c) 2016, Christian Albinsson

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
