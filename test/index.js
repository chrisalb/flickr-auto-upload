var assert    = require('chai').assert;
var expect    = require('chai').expect;
var fs        = require('fs');
var find      = require("../modules/find.js");

var should = require('chai').should();

/*describe('#dir', function() {
  it('Check abort on wrong dir', function(done) {
    error = null;
      try {
        dir.dive('asdasd', function(){ });
      } catch (e){
         error = e;
      }

      assert( error != null, 'Did not abort on wrong dir');
      done();
  });
});*/

describe('#check_for_change', function() {

  it('Check watch module exists', function(done) {
      uploadOpts = {};
      uploadOpts.path = './';
      uploadOpts.uploader = function (a){ console.log(a) };

      var error = null;
      try {
        finder = new find();
      } catch (e){
        error = e;
      }

/*
      finder.listenForChanges();
      fs.closeSync(fs.openSync('temp.test', 'w'));*/
      assert( error == null, 'Watch module not found');
      done();
  });
});
