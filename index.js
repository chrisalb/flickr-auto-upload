/**
 * Watch images folder and automatically upload to Flickr
 */

const flickr_upload = require("./modules/flickr.js");
const findFiles = require("./modules/find_files.js");

module.exports = {
	set_config:  function (config) {
		flickr_upload.set_config(config);
	},

	watch: function (opts){
		opts.flickr = flickr_upload;
		return new findFiles(opts);
	}
}