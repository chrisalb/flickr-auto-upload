var chokidar = require('chokidar');

var findFiles = function(opts) {
	if (!opts) {
		throw new Error('oops: no configuration, must exit');
	}

	this.path = opts.path;
	this.flickr = opts.flickr;
    this.flickr.set_config(opts.flickr.config);

	this.watcher = chokidar.watch(this.path, {
		ignored: /[\/\\]\./,
		ignoreInitial: true,
		persistent: true
	});


	this.listenForChanges();

};

findFiles.prototype = {

	listenForChanges: function() {
		self = this;
		this.watcher
			.on('change', function(path) {
				self.flickr.upload(path);
			})
			.on('add', function(path) {
				self.flickr.upload(path);
			});
	},

};

module.exports = findFiles;