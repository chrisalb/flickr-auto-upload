var path = require("path");
var Flickrapi = require("flickrapi");
var fs = require('fs');
var request = require('request');
var Promise = require('bluebird');
var ProgressBar = require('progress');
var notifier = require('node-notifier');

var flickr = function () {
    this.opts = {};
    this.opts.pages = 1;
    this.opts.per_page = 20;
    this.config = {};
    this.totallength = 0;

    self = this;
}


flickr.prototype.set_config = function (config) {
    this.config = config;
}

flickr.prototype.set_opt = function (opt, value) {
    this.opts.opt = value;
}

flickr.prototype.promiseWhile = function (condition, action) {
    var resolver = Promise.defer();

    var loop = function () {
        if (!condition()) return resolver.resolve();
        return Promise.cast(action())
            .then(loop)
            .catch(resolver.reject);
    };

    process.nextTick(loop);

    return resolver.promise;
}

flickr.prototype.requestDownloadFile = function (results, labelfolder) {
    var index = 0;
    var stop = results.length;
    var bar = new ProgressBar(':bar', {total: 10});

    this.promiseWhile(function () {
        return index < stop;
    }, function () {

        return new Promise(function (resolve, reject) {
            url = results[index];

            request.get({url: url, encoding: 'binary'}, function (err, response, body) {

                labelpath = '' + labelfolder;

                if (!fs.existsSync(labelpath)) {
                    fs.mkdirSync(labelpath);
                    fs.chmodSync(labelpath, '777');
                }
                fs.writeFile(labelpath + '/' + path.basename(url), body, 'binary', function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        resolve();
                    }

                });

            }).on('response', function (response) {
                    total = parseInt(response.headers['content-length']);
                    var bar = new ProgressBar('  downloading ' + path.basename(url) + ' [:bar] :percent :etas', {
                        complete: '=',
                        incomplete: ' ',
                        width: 20,
                        total: total
                    });


                    response.on('data', function (data) {
                        bar.tick(data.length);
                    })
                });

            index++;
        });
    }).then(function () {
        request.get('', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log('generating thumbnails');
            } else {
                console.log(error);
            }
        }).on('end',function (end) {

            console.log('** all done **');
            process.exit(0);

        });

    });
}

flickr.prototype.search = function (query, labelfolder) {

    var index = 0;
    var total = 0;
    var page = 1;
    var pages = this.opts.pages;
    var per_page = this.opts.per_page;
    var stop = (pages * per_page);

    results = [];
    var self = this;

    this.promiseWhile(function () {


        return total < stop;
    }, function () {

        // Action to run, should return a promise
        return new Promise(function (resolve, reject) {


            Flickrapi.authenticate(self.config, function (err, flickrSearch) {


                searchopts = self.config;
                searchopts = {};
                searchopts.text = query;
                searchopts.extras = 'url_z';
                searchopts.sort = 'relevance';
                searchopts.per_page = per_page;
                searchopts.page = page;


                flickrSearch.photos.search(
                    searchopts
                    , function (err, res) {

                        if (total > 0 && (total % per_page ) == 0) {
                            page++;
                            index = 0;
                        }

                        total++;

                        if (res.photos.photo[index].url_z != undefined) {
                            console.log(res.photos.photo[index].url_z);
                            results.push(res.photos.photo[index].url_z);
                        }

                        resolve();
                        index++;


                    })
                ;

            });
        });
    }).then(function () {
        return self.requestDownloadFile(results, labelfolder);
    });

}

flickr.prototype.upload = function (photo_path) {

    var self = this;

    var filename = path.basename(photo_path);

    var uploadOptions = {
        photos: [{
            title: filename,
            is_public: 0,
            is_friend: 0,
            is_family: 0,
            photo: photo_path,
            tags: []
        }]
    };

    try {
        Flickrapi.upload(uploadOptions, self.config, function (err, result) {
            if (err) {

                notifier.notify({
                    'title': 'Flickr upload error',
                    'message': err
                });

                return;
            }

            notifier.notify({
                'title': 'Flickr upload',
                'message': 'Uploaded: ' + filename + '\n(Id: ' + result + ')'
            });

        });
    } catch (e) {
        notifier.notify({
            'title': 'Flickr upload error',
            'message': e
        });

        process.exit(1);
    }

}


module.exports = new flickr;