var flickrUpload		 	= require("./index.js");

flickrUpload.set_config(
	{
		api_key: "",
		secret: "",
		access_token: "",
		access_token_secret: ""
	}
);
flickrUpload.watch(
	{
		path: 	'/path/to/images',
	}
);
